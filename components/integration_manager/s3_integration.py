import boto3

from config import get_config_base

def locate_s3_bucket(logical_bucket_name):

    aws_systems_manager = boto3.client('ssm')
    config_base = get_config_base()
    bucket_name_parameter = aws_systems_manager.get_parameter(
        Name="{}/s3BucketNames/{}".format(config_base, logical_bucket_name))
    return bucket_name_parameter["Parameter"]["Value"]
