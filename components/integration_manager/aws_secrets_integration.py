import json

import boto3
from botocore.exceptions import ClientError

import logging
from exceptions import ValidationException
from aws_resource_locator import locate_aws_secret

_LOGGER = logging.get_logger(__name__)


def get_secret(logical_secret_name):

    secret_arn = locate_aws_secret(logical_secret_name)
    if not isinstance(secret_arn, str):
        raise ValidationException("Could not locate the AWS secret named \"{name}.\"".format(name=logical_secret_name))
    secrets_manager = boto3.client("secretsmanager")
    try:
        secrets_manager_response = secrets_manager.get_secret_value(SecretId=secret_arn)
    except ClientError as e:
        _LOGGER.error(
            "Failed to retrieve AWS secret named \"%s\" (ARN \"%s\"): %s",
            logical_secret_name,
            secret_arn,
            e
        )
        return None
    secret_json = secrets_manager_response.get("SecretString")
    if isinstance(secret_json, str):
        return json.loads(secret_json)
    else:
        _LOGGER.warning(
            "AWS secret \"%s\" isn't a string.",
            secret_arn
        )
